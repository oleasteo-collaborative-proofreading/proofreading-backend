import {randomBytes} from "crypto";
import {simpleHash} from "./src/services/crypto";

/*===================================================== Exports  =====================================================*/

export default {
  port: 8642,
  serverId: simpleHash(process.pid + "" + Date.now() + randomBytes(2).toString("hex"), "hex").substring(0, 8),
};
