// This file needs to be kept the same as within frontend src/constants/document-schema.js
import pmSchema from "prosemirror-schema-basic";

/*===================================================== Exports  =====================================================*/

export default pmSchema.schema;
