import {start as startServer} from "./services/socket/server";
import {app as logger} from "./services/logger";
import attachRoutes from "./routes";

/*================================================ Initial execution  ================================================*/

attachRoutes();

const server = startServer();

server.on("error", (err) => logger.err(err));
server.on("listening", () => logger.info("server listening"));
