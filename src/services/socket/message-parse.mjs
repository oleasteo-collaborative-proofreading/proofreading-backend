const MAX_MESSAGE_LENGTH = 1024 * 1024;

/*===================================================== Exports  =====================================================*/

export { validateIncomingMessage, parseMessage };

/*==================================================== Functions  ====================================================*/

function validateIncomingMessage(messageString) {
  if (messageString.length > MAX_MESSAGE_LENGTH) { throw new Error("Message too long."); }
}

// queryId|0, type, [payload]
function parseMessage(messageString, _id) {
  let messageArray = null;
  try {
    messageArray = JSON.parse(`[${ messageString }]`);
  } catch (err) {
    throw new Error("Message is no valid comma-separated JSON.");
  }
  if (!Array.isArray(messageArray)) {
    throw new Error("Message must be an Array.");
  }

  const [ queryId, type, payload ] = messageArray;

  if (typeof queryId !== "number" || queryId < 0) {
    throw new Error("Message[0] (query-id | 0) must be a non-negative integer.");
  }
  if (typeof type !== "string") {
    throw new Error("Message[1] (type) must be a string.");
  }

  if (queryId === 0) {
    return { _id, isQuery: false, type, payload };
  } else {
    return { _id, isQuery: true, queryId, type, payload };
  }
}
