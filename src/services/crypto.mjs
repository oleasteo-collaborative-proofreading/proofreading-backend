import {createHash} from "crypto";

const ID_SCOPE_BITS = 48;
const CAP_ID_SCOPE = 2 ** ID_SCOPE_BITS;
const CAP_ID_SCOPE_STEP = 2 ** (ID_SCOPE_BITS / 2);

let nextIdScope = (Math.random() * CAP_ID_SCOPE) | 0;

/*===================================================== Exports  =====================================================*/

export {
  simpleHash,
  getNextIdScope,
};

/*==================================================== Functions  ====================================================*/

function simpleHash(input, outputType) {
  const hashSHA256 = createHash("sha512");
  hashSHA256.update(input);
  return hashSHA256.digest(outputType);
}

function getNextIdScope({serverId}) {
  // Result: [3xHEX scopeID]-[serverID]-[scopeID]-[1xHEX scopeID]
  // Clients may complete as following: [1xHEX modelID][Result][3xHEX modelID]
  const idScope = nextIdScope; // at least 5 HEX entropy
  nextIdScope += (Math.random() * CAP_ID_SCOPE_STEP) | 0;
  if (nextIdScope > CAP_ID_SCOPE) { nextIdScope -= CAP_ID_SCOPE; }
  const idScopeString = idScope.toString(16).padStart(ID_SCOPE_BITS / 4, "0");
  const breakPointBase = (ID_SCOPE_BITS / 4) - 4;
  return ("" // some simple scrambling to make first and last block contain high scope entropy
      + idScopeString.charAt(breakPointBase + 2)
      + idScopeString.charAt(breakPointBase + 1)
      + idScopeString.charAt(breakPointBase) + "-"
      + serverId + "-"
      + idScopeString.substr(0, breakPointBase) + "-"
      + idScopeString.charAt(breakPointBase + 3)
  );
}
