import oddlog from "oddlog";
import config from "./config";

/*===================================================== Exports  =====================================================*/

export const app = oddlog.createLogger("app", null, {config});
export const client = oddlog.createLogger("client", {
  transformers: Object.assign(
      oddlog.stdTransformer,
      {
        session(session) { return session._id; }
      }
  )
});
