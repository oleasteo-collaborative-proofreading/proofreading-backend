import { client as logger } from "../services/logger";

let nextId = 0;

/*===================================================== Exports  =====================================================*/

export default class Session {

  constructor(socket) {
    this._id = nextId++;
    this.socket = socket;
    this.logger = logger.child({ session: this });
  }

  // type, [payload]
  notify(type, payload) {
    if (payload === void 0) {
      this.socket.send(`"${ type }"`);
    } else {
      this.socket.send(`"${ type }",${ JSON.stringify(payload) }`);
    }
  }

  // "error", [payload]
  notifyError(error) {
    if (typeof error === "string") {
      this.socket.send(`"error",{"reason":${ JSON.stringify(error) }}`);
    } else if (error === void 0) {
      this.socket.send(`"error"`);
    } else {
      this.socket.send(`"error",${ JSON.stringify(error) }`);
    }
  }

  // id, [payload], [status=200]
  respondSuccess(queryId, payload, status = 200) {
    if (status === 200) {
      if (payload === void 0) {
        this.socket.send(`${ queryId }`);
      } else {
        this.socket.send(`${ queryId },${ JSON.stringify(payload) }`);
      }
    } else {
      if (payload === void 0) {
        this.socket.send(`${ queryId },${ status }`);
      } else {
        this.socket.send(`${ queryId },${ JSON.stringify(payload) },${ status }`);
      }
    }
  }

  // id, [payload], status
  respondError(queryId, error, status = 500) {
    if (typeof error === "string") {
      this.socket.send(`${ queryId },{"reason":${ JSON.stringify(error) }},${ status }`);
    } else if (error === void 0) {
      this.socket.send(`${ queryId },${ status }`);
    } else {
      this.socket.send(`${ queryId },${ JSON.stringify(error) },${ status }`);
    }
  }

}
