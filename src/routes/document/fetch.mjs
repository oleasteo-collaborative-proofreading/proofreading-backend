import {getDocumentBlob} from "./memory";

/*===================================================== Exports  =====================================================*/

export const notifications = {};
export const queries = {fetch};

/*==================================================== Functions  ====================================================*/

function fetch({payload: {documentId, baseVersion}}) { return getDocumentBlob(documentId, baseVersion); }
