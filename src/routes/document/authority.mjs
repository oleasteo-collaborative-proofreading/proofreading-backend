import ClientError from "../../classes/ClientError";
import {applySteps, documentExists, getDocumentMemory} from "./memory";

/*===================================================== Exports  =====================================================*/

export const notifications = {subscribe, unsubscribe};
export const queries = {steps: receiveSteps};

/*==================================================== Functions  ====================================================*/

async function receiveSteps({payload: {documentId, baseVersion, steps}, session}) {
  if (typeof documentId !== "string") { throw new ClientError(400, "No document ID provided."); }
  if (!documentExists(documentId)) { throw new ClientError(404, "Document does not exist."); }
  applySteps(documentId, {baseVersion, steps});
  for (let subscriber of getDocumentMemory(documentId).subscribers) {
    if (subscriber !== session) { subscriber.notify("document:steps", {documentId, baseVersion, steps}); }
  }
}

function subscribe({payload: {documentId}, session}) {
  if (typeof documentId !== "string") { throw new ClientError(400, "No document ID provided."); }
  if (!documentExists(documentId)) { throw new ClientError(404, "Document does not exist."); }
  getDocumentMemory(documentId).subscribers.push(session);
  session.socket.on("close", unsubscribe.bind(null, {payload: {documentId}, session}));
}

function unsubscribe({payload: {documentId}, session}) {
  if (typeof documentId !== "string") { throw new ClientError(400, "No document ID provided."); }
  if (!documentExists(documentId)) { throw new ClientError(404, "Document does not exist."); }
  const subscribers = getDocumentMemory(documentId).subscribers;
  const idx = subscribers.indexOf(session);
  if (~idx) { subscribers.splice(idx, 1); }
}
