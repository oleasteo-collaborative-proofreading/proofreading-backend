// TODO everything stored here should move into database at some point in time...

import pmModel from "prosemirror-model";
import pmTransform from "prosemirror-transform";
import config from "../../services/config";
import schema from "../../constants/document-schema";
import ClientError from "../../classes/ClientError";
import {getNextIdScope} from "../../services/crypto";

const Node = pmModel.Node;
const Step = pmTransform.Step;
const documents = {};
const documentMemory = {};

/*===================================================== Exports  =====================================================*/

export {
  documentExists, createDocument,
  getDocumentBlob, applySteps,
  getDocumentMemory,
};

/*==================================================== Functions  ====================================================*/

function documentExists(id) { return documents.hasOwnProperty(id); }

function getDocumentData(id) { return documents[id]; }

function getDocumentMemory(id) { return documentMemory[id]; }

function setDocumentData(id, data) { documents[id] = data; }

function setDocumentMemory(id, data) { documentMemory[id] = data; }

function createDocument(id) {
  if (documents.hasOwnProperty(id)) { throw new ClientError(405, "Document data already exists."); }
  setDocumentData(id, {
    doc: Node.fromJSON(schema, {type: "doc", content: [{type: "paragraph"}]}), // keep in sync with client creation
    version: 0,
    inverseSteps: [/*Step*/],
  });
  setDocumentMemory(id, {
    subscribers: [/*Session*/],
  });
}

function getDocumentBlob(id, versionRequest) {
  if (!documents.hasOwnProperty(id)) { throw new ClientError(404, "Document does not exist."); }
  const {doc, version, inverseSteps} = documents[id];
  let localInverseSteps = null;
  if (versionRequest >= 0) {
    const minVersion = version - inverseSteps.length;
    if (versionRequest > version || versionRequest < minVersion) {
      throw new ClientError(400, "Document has no such version.", {versionRequest, minVersion, maxVersion: version});
    }
    localInverseSteps = inverseSteps.slice(versionRequest - minVersion);
  }
  return {
    id,
    doc, version, inverseSteps: localInverseSteps || [],
    idScope: getNextIdScope(config),
    comments: [
      {
        id: "00-2f",
        from: "Brosenne Brosennem",
        tags: ["Lorem", "Structure"],
        body: "Earth's Mightiest Heroes joined forces to take on threats that were too big for any one hero to " +
            "tackle...",
      },
    ],
    suggestions: [],
  };
}

function applySteps(documentId, {baseVersion, steps}) {
  const {doc, version, inverseSteps} = getDocumentData(documentId);
  if (baseVersion !== version) {
    throw new ClientError(409, "Document version mismatch.", {version, versionRequest: baseVersion});
  }
  const localInverseSteps = [];
  let currentDoc = doc;
  for (let i = 0; i < steps.length; i++) {
    const step = Step.fromJSON(schema, steps[i]);
    // eslint-disable-next-line prefer-reflect
    const result = step.apply(currentDoc);
    if (result.failed) {
      throw new ClientError(409, "Failed to apply step.", {step: steps[i], index: i, reason: result.failed});
    }
    localInverseSteps.push(step.invert(currentDoc));
    currentDoc = result.doc;
  }
  setDocumentData(documentId, {
    doc: currentDoc,
    version: version + steps.length,
    inverseSteps: inverseSteps.concat(localInverseSteps)
  });
}
