import {onNotification, onQuery} from "../services/socket/bus";
import document from "./document";

/*===================================================== Exports  =====================================================*/

export default attachRoutes;

/*==================================================== Functions  ====================================================*/

function attachRoutes() {
  register(document);
}

function register({prefix, queries, notifications}) {
  for (let key in queries) { // noinspection JSUnfilteredForInLoop
    onQuery(prefix + key, queries[key]);
  }
  for (let key in notifications) { // noinspection JSUnfilteredForInLoop
    onNotification(prefix + key, notifications[key]);
  }
}
